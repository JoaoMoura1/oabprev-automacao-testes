package com.javarestassuredtemplate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GlobalParameters {
    public static String RESOURCE_PATH;
    public static String ENVIROMENT;
    public static String URL_RD_STATION;
    public static String TOKEN_RD_STATION;
    public static String URL_STOCKPREV;
    public static String URL_OABPREV;
    public static String REPORT_NAME;
    public static String REPORT_PATH;
    public static String AUTHENTICATOR_USER;
    public static String AUTHENTICATOR_PASSWORD;
    public static String SFTP_HOST;
    public static String SFTP_USER;
    public static String SFTP_PASSWORD;
    public static String SFTP_PORT;

    private Properties properties;
    public Properties keys;

    public GlobalParameters(){
        properties = new Properties();
        keys = new Properties();

        InputStream input = null;
        try {
            input = new FileInputStream("globalParameters.properties");
            properties.load(input);

            input = new FileInputStream("keys.properties");
            keys.load(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        RESOURCE_PATH = properties.getProperty("resourcePath");
        REPORT_NAME = properties.getProperty("report.name");
        REPORT_PATH = properties.getProperty("report.path");
        ENVIROMENT = properties.getProperty("enviroment");

        if(ENVIROMENT.equals("hml")) {
            AUTHENTICATOR_PASSWORD = properties.getProperty("hml.authenticator.user");
            AUTHENTICATOR_USER = properties.getProperty("hml.authenticator.password");

            URL_RD_STATION = properties.getProperty("hml.rdStation.url");
            TOKEN_RD_STATION = keys.getProperty("hml.rdStation.token");

            URL_STOCKPREV = properties.getProperty("hml.stockprev.url");

            URL_OABPREV = properties.getProperty("hml.oabprev.url");

            SFTP_HOST = keys.getProperty("hml.sftp.host");
            SFTP_USER = keys.getProperty("hml.sftp.user");
            SFTP_PASSWORD = keys.getProperty("hml.sftp.password");
            SFTP_PORT = keys.getProperty("hml.sftp.port");
        }

    }
}
