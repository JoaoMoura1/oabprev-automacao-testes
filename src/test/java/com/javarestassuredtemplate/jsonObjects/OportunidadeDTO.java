package com.javarestassuredtemplate.jsonObjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OportunidadeDTO {
    private String nome;
    private String cpf;
    private Date dataNascimento;
    private String sexo;
    private String cidade;
    private String estado;
    private String estadoCivil;
    private String rendaMensal;
    private String telefone1;
    private String telefone2;
    private String email1;
    private String email2;
    private String matriculaPlano;
    private Date dataInscricaoPlano;
    private String criterioTributacaoPlano;
    private String contribuicaoVigenteAposentadoria;
    private String contribuicaoPensaoInvalidez;
    private String contribuicaoPensaoMorte;
    private String totalContribuicaoMensalVigente;
    private String situacao;
    private String statusParticipante;
    private Date dataCancelamento;
    private String valorFundoAcumulado;
    private String numeroProposta;
    private String formaPagamento;
    private String diaVencimento;
    private String corretor;
}
