package com.javarestassuredtemplate.utils;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.jsonObjects.OportunidadeDTO;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class OportunidadeUtils {
    public static List<OportunidadeDTO> obterOportunidadesCSV(String path) {
        List<OportunidadeDTO> participantes = new ArrayList<>();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String line = "";
        try {
            String filePath = GlobalParameters.RESOURCE_PATH + path;
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            br.readLine();

            while ((line = br.readLine()) != null) {
                OportunidadeDTO participante = new OportunidadeDTO();

                String[] values = line.split(",");

                participante.setSituacao(values[0]);
                participante.setStatusParticipante(values[1]);
                participante.setMatriculaPlano(values[2]);
                if(!values[3].isEmpty()) {
                    participante.setDataInscricaoPlano(simpleDateFormat.parse(values[3]));
                }
                participante.setNumeroProposta(values[4]);
                participante.setCpf(values[5]);
                participante.setNome(values[6]);
                if(!values[7].isEmpty()) {
                    participante.setDataCancelamento(simpleDateFormat.parse(values[7]));
                }
                if(!values[8].isEmpty()) {
                    participante.setDataNascimento(simpleDateFormat.parse(values[8]));
                }
                participante.setRendaMensal(values[9]);
                participante.setSexo(values[10]);
                participante.setEstadoCivil(values[11]);
                participante.setEmail1(values[12]);
                participante.setEmail2(values[13]);
                participante.setCidade(values[14]);
                participante.setEstado(values[15]);
                participante.setTelefone1(values[16]);
                participante.setTelefone2(values[17]);
                participante.setCriterioTributacaoPlano(values[20]);
                participante.setContribuicaoVigenteAposentadoria(values[21]);
                participante.setContribuicaoPensaoInvalidez(values[22]);
                participante.setContribuicaoPensaoMorte(values[23]);
                participante.setTotalContribuicaoMensalVigente(values[24]);
                participante.setValorFundoAcumulado(values[25]);
                participante.setFormaPagamento(values[26]);
                participante.setDiaVencimento(values[27]);
                participante.setCorretor(values[28]);

                participantes.add(participante);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        return participantes;
    }
}
