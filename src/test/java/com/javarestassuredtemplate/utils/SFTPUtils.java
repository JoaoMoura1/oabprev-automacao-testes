package com.javarestassuredtemplate.utils;

import com.javarestassuredtemplate.connections.SFTPConnection;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class SFTPUtils {

    static List<SFTPConnection> sftpConnections = new ArrayList<>();

    public static SFTPConnection getConnection(
            String user,
            String host,
            String port,
            String password
    ) {
        SFTPConnection sftpConnection = new SFTPConnection();

        Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");

        try {
            JSch jSch = new JSch();

            Session session = jSch.getSession(user, host, Integer.valueOf(port));
            session.setPassword(password);
            session.setConfig(config);
            session.connect();

            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            System.out.println("Session connected " + session.isConnected());

            channelSftp.connect();
            System.out.println("Sftp Channel connected");

            sftpConnection.setJSch(jSch);
            sftpConnection.setSession(session);
            sftpConnection.setChannelSftp(channelSftp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(sftpConnections.isEmpty()) {
            sftpConnections = new ArrayList<>();
        }
        sftpConnections.add(sftpConnection);

        return sftpConnection;
    }

    public static void disconnectAll() {
        if(sftpConnections.isEmpty()) {
            return;
        }

        for(SFTPConnection sftpConnection : sftpConnections) {
            sftpConnection.disconnect();
        }

        sftpConnections = new ArrayList<>();
    }
}
