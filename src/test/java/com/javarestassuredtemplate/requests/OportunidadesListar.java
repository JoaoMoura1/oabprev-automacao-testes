package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class OportunidadesListar extends RequestRestBase {
    public OportunidadesListar(){
        url = GlobalParameters.URL_RD_STATION;
        requestService = "/deals";
        method = Method.GET;

        queryParameters.put("token", GlobalParameters.TOKEN_RD_STATION);
    }

    public void setDealStageId(String dealStageId){
        queryParameters.put("deal_stage_id", dealStageId);
    }

    public void setExactName(Boolean exactName) {
        queryParameters.put("exact_name", exactName.toString());
    }

    public void setName(String name) {
        queryParameters.put("name", name);
    }
}
