package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class Participante extends RequestRestBase {

    public Participante(){
        url = GlobalParameters.URL_OABPREV;
        requestService = "/participante";
        method = Method.GET;
    }
}
