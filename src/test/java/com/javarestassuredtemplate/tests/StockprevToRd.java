package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.connections.SFTPConnection;
import com.javarestassuredtemplate.jsonObjects.OportunidadeDTO;
import com.javarestassuredtemplate.requests.OportunidadesListar;
import com.javarestassuredtemplate.requests.Participante;
import com.javarestassuredtemplate.steps.FluxoCadastrarParticipantes;
import com.javarestassuredtemplate.utils.OportunidadeUtils;
import com.javarestassuredtemplate.utils.GeneralUtils;
import com.javarestassuredtemplate.utils.SFTPUtils;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import static org.hamcrest.Matchers.*;

import org.exparity.hamcrest.date.DateMatchers;
import org.testng.annotations.Test;

import java.util.List;

public class StockprevToRd extends TestBase {
    Participante participante;
    OportunidadesListar oportunidadesListar;
    FluxoCadastrarParticipantes fluxoCadastrarParticipantes;

    @Test
    public void rodarFluxoParticipante () {
        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;
        long maxResponseTimeMS = 10000;

        SFTPConnection sftpConnection = SFTPUtils.getConnection(
                GlobalParameters.SFTP_USER,
                GlobalParameters.SFTP_HOST,
                GlobalParameters.SFTP_PORT,
                GlobalParameters.SFTP_PASSWORD
        );

        //Fluxo
        sftpConnection.putFile("/stockprevToRd/rodarFluxoParticipante/Teste_carga_RD.csv",
                "/home/base2/files");

        participante = new Participante();
        ValidatableResponse response = participante.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.time(lessThan(maxResponseTimeMS));
    }

    @Test
    public void cadastrarParticipantes () {
        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        String dealStageId = "626047dc50c89f000df739e7";

        List<OportunidadeDTO> oportunidades = OportunidadeUtils.obterOportunidadesCSV(
                "/stockprevToRd/cadastrarParticipantes/Teste_carga_RD.csv");

        //Fluxo
        SFTPConnection sftpConnection = SFTPUtils.getConnection(
                GlobalParameters.SFTP_USER,
                GlobalParameters.SFTP_HOST,
                GlobalParameters.SFTP_PORT,
                GlobalParameters.SFTP_PASSWORD
        );

        sftpConnection.putFile("/stockprevToRd/cadastrarParticipantes/Teste_carga_RD.csv",
                "/home/base2/files");

        participante = new Participante();
        participante.executeRequest();

        for(OportunidadeDTO oportunidade : oportunidades) {
            oportunidadesListar = new OportunidadesListar();
            oportunidadesListar.setDealStageId(dealStageId);
            oportunidadesListar.setExactName(true);
            oportunidadesListar.setName(oportunidade.getNome());
            ValidatableResponse response = oportunidadesListar.executeRequest();

            //Asserções
            response.statusCode(statusCodeEsperado);
            response.body("deals[0].deal_custom_fields.find{it.custom_field.label=='Situação'}.value", equalTo(oportunidade.getSituacao()),
                      "deals[0].deal_custom_fields.find{it.custom_field.label=='Status Participante'}.value", equalTo(oportunidade.getStatusParticipante()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Matrícula no Plano'}.value", equalTo(oportunidade.getMatriculaPlano()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data Inscrição no Plano'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataInscricaoPlano(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Número Proposta'}.value", equalTo(oportunidade.getNumeroProposta()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='CPF/CNPJ'}.value", equalTo(oportunidade.getCpf()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data Cancelamento'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataCancelamento(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data de Nascimento'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataNascimento(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Renda Mensal'}.value", equalTo(oportunidade.getRendaMensal()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Sexo'}.value", equalTo(oportunidade.getSexo()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Estado Civil'}.value", equalTo(oportunidade.getEstadoCivil()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='E-mail'}.value", equalTo(oportunidade.getEmail1()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='E-mail 2'}.value", equalTo(oportunidade.getEmail2()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Cidade'}.value", equalTo(oportunidade.getCidade()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Estado'}.value", equalTo(oportunidade.getEstado()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Telefone 1'}.value", equalTo(oportunidade.getTelefone1()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Telefone 2'}.value", equalTo(oportunidade.getTelefone2()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Critério de Tributação no Plano'}.value", equalTo(oportunidade.getCriterioTributacaoPlano()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Vigente Aposentadoria'}.value", equalTo(oportunidade.getContribuicaoVigenteAposentadoria()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Pensão Invalidez'}.value", equalTo(oportunidade.getContribuicaoPensaoInvalidez()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Pensão Morte'}.value", equalTo(oportunidade.getContribuicaoPensaoMorte()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Total Contribuição Mensal Vigente'}.value", equalTo(oportunidade.getTotalContribuicaoMensalVigente()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Valor do Fundo Acumulado'}.value", equalTo(oportunidade.getValorFundoAcumulado()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Forma Pagamento'}.value", equalTo(oportunidade.getFormaPagamento()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Dia Vencimento'}.value", equalTo(oportunidade.getDiaVencimento()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Corretor'}.value", equalTo(oportunidade.getCorretor()));
        }
    }

    @Test
    public void atualizarParticipantes () {
        //Parâmetros
        int statusCodeEsperado = HttpStatus.SC_OK;

        String dealStageId = "626047dc50c89f000df739e7";

        List<OportunidadeDTO> oportunidades = OportunidadeUtils.obterOportunidadesCSV(
                "/stockprevToRd/atualizarParticipantes/Teste_carga_RD.csv");

        fluxoCadastrarParticipantes = new FluxoCadastrarParticipantes();

        //Fluxo
        fluxoCadastrarParticipantes.cadastrarParticipantes(
                "/stockprevToRd/atualizarParticipantes/fluxoCadastro/Teste_carga_RD.csv");

        SFTPConnection sftpConnection = SFTPUtils.getConnection(
                GlobalParameters.SFTP_USER,
                GlobalParameters.SFTP_HOST,
                GlobalParameters.SFTP_PORT,
                GlobalParameters.SFTP_PASSWORD
        );

        sftpConnection.putFile("/stockprevToRd/atualizarParticipantes/Teste_carga_RD.csv",
                "/home/base2/files");

        participante = new Participante();
        participante.executeRequest();

        for(OportunidadeDTO oportunidade : oportunidades) {
            oportunidadesListar = new OportunidadesListar();
            oportunidadesListar.setDealStageId(dealStageId);
            oportunidadesListar.setExactName(true);
            oportunidadesListar.setName(oportunidade.getNome());
            ValidatableResponse response = oportunidadesListar.executeRequest();

            //Asserções
            response.statusCode(statusCodeEsperado);
            response.body("deals[0].deal_custom_fields.find{it.custom_field.label=='Situação'}.value", equalTo(oportunidade.getSituacao()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Status Participante'}.value", equalTo(oportunidade.getStatusParticipante()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Matrícula no Plano'}.value", equalTo(oportunidade.getMatriculaPlano()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data Inscrição no Plano'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataInscricaoPlano(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Número Proposta'}.value", equalTo(oportunidade.getNumeroProposta()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='CPF/CNPJ'}.value", equalTo(oportunidade.getCpf()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data Cancelamento'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataCancelamento(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Data de Nascimento'}.value",
                    equalTo(GeneralUtils.dateToString(oportunidade.getDataNascimento(), "dd/MM/yyyy")),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Renda Mensal'}.value", equalTo(oportunidade.getRendaMensal()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Sexo'}.value", equalTo(oportunidade.getSexo()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Estado Civil'}.value", equalTo(oportunidade.getEstadoCivil()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='E-mail'}.value", equalTo(oportunidade.getEmail1()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='E-mail 2'}.value", equalTo(oportunidade.getEmail2()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Cidade'}.value", equalTo(oportunidade.getCidade()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Estado'}.value", equalTo(oportunidade.getEstado()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Telefone 1'}.value", equalTo(oportunidade.getTelefone1()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Telefone 2'}.value", equalTo(oportunidade.getTelefone2()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Critério de Tributação no Plano'}.value", equalTo(oportunidade.getCriterioTributacaoPlano()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Vigente Aposentadoria'}.value", equalTo(oportunidade.getContribuicaoVigenteAposentadoria()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Pensão Invalidez'}.value", equalTo(oportunidade.getContribuicaoPensaoInvalidez()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Contribuição Pensão Morte'}.value", equalTo(oportunidade.getContribuicaoPensaoMorte()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Total Contribuição Mensal Vigente'}.value", equalTo(oportunidade.getTotalContribuicaoMensalVigente()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Valor do Fundo Acumulado'}.value", equalTo(oportunidade.getValorFundoAcumulado()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Forma Pagamento'}.value", equalTo(oportunidade.getFormaPagamento()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Dia Vencimento'}.value", equalTo(oportunidade.getDiaVencimento()),
                    "deals[0].deal_custom_fields.find{it.custom_field.label=='Corretor'}.value", equalTo(oportunidade.getCorretor()));
        }
    }
}
