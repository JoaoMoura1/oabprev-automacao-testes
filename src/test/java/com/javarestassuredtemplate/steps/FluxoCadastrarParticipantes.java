package com.javarestassuredtemplate.steps;

import com.javarestassuredtemplate.GlobalParameters;
import com.javarestassuredtemplate.connections.SFTPConnection;
import com.javarestassuredtemplate.requests.Participante;
import com.javarestassuredtemplate.utils.SFTPUtils;

public class FluxoCadastrarParticipantes {

    Participante participante;

    public void cadastrarParticipantes(String filePath) {
        SFTPConnection sftpConnection = SFTPUtils.getConnection(
                GlobalParameters.SFTP_USER,
                GlobalParameters.SFTP_HOST,
                GlobalParameters.SFTP_PORT,
                GlobalParameters.SFTP_PASSWORD
        );

        sftpConnection.putFile(filePath,
                "/home/base2/files");

        SFTPUtils.disconnectAll();

        participante = new Participante();
        participante.executeRequest();
    }
}
