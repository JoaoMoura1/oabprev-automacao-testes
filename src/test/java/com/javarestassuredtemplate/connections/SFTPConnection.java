package com.javarestassuredtemplate.connections;

import com.javarestassuredtemplate.GlobalParameters;
import com.jcraft.jsch.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SFTPConnection {
    JSch jSch;
    Session session;
    ChannelSftp channelSftp;

    public void disconnect() {
        getChannelSftp().disconnect();
        getSession().disconnect();

        System.out.println("Session connected " + getSession().isConnected());
    }

    public void putFile(String source, String destiny) {
        try {
            System.out.println("uploading file...");

            getChannelSftp().put(GlobalParameters.RESOURCE_PATH + source, destiny);

            System.out.println("file uploaded");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
